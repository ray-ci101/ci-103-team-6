from django.shortcuts import render, redirect, HttpResponse
from django.http import request
from util import pachong
# Create your views here.

import re
from pymysql import *

def index(request):

    return render(request, "index.html",locals());

def result(request):
    url = request.POST.get('url', '')
    print(url)
    url1 = url

    index1 = re.search('id', url1).span()  # 检索用户输入URL中的商品id索引
    if index1[-1] > 70:  # 用以判断该商品来源为天猫还是淘宝（天猫淘宝所需的header不同）
        index2 = index1[-1] + 1
        list1 = list(url1)
        id = ''
        for i in range(index2, index2 + 12):
            id += list1[i]  # 循环以获取商品id
        taobao = pachong.TaoBao(id)
        k = taobao.getAllData()
        k = str(k)
        taobaocomments = re.findall(r'\'content\': \'(.+?)\',', k)  # 正则表达式匹配所需的商品评论
        # taobaocomments = taobaocomments.replaceAll("[\\x{10000}-\\x{10FFFF}]", "")  # 去除评论的表情符号，以免导入数据库出错
        # print(taobaocomments)
        informations = re.findall(r'\'sku\': \'(.+?)\',', k)  # 正则表达式匹配所需的商品种类
        # print(informations)
        time = re.findall(r'\'date\': \'(.+?)\',', k)  # 正则表达式匹配所需的评论日期
        # print(time)
        username = re.findall(r'\'nick\': \'(.+?)\',', k)  # 正则表达式匹配所需的用户名称
        # print(username)

    else:
        index2 = index1[-1] + 1
        list1 = list(url1)
        id = ''
        for i in range(index2, index2 + 12):
            id += list1[i]
        tianmao = pachong.TianMao(id)
        k = tianmao.getAllData()
        k = str(k)
        tianmaocomments = re.findall(r'\'rateContent\': \'(.+?)\',', k)
        # tianmaocomments = tianmaocomments.replaceAll("[\\x{10000}-\\x{10FFFF}]", "")
        # print(tianmaocomments)
        informations = re.findall(r'\'auctionSku\': \'(.+?)\',', k)
        # print(informations)
        time = re.findall(r'\'rateDate\': \'(.+?)\',', k)
        # print(time)
        username = re.findall(r'\'displayUserNick\': \'(.+?)\',', k)
        # print(username)


    conn = connect(host='localhost', port=3306, user='root', password='root', database='fakespy',charset='utf8mb4')
    cur = conn.cursor()  ##获取游标

    # sql = ' insert into wordcloud (keyword,EMO) values(%s,%s)'
    print(tianmaocomments)
    cur.execute('truncate table orindata')
    for p in range(0, len(username)):
        cur.execute('insert into orindata (comment,productype,cTime,username) values(%s,%s,%s,%s)',
                    (tianmaocomments[p], informations[p], time[p], username[p]))
    conn.commit()
    conn.close()
    print('OK')

    #判断
    from fuzzywuzzy import fuzz
    sum = ""
    for aa in tianmaocomments:
        sum += str(aa)
    num_sum = int(0.5 * len(sum))
    sum1 = ''
    sum2 = ''
    sum1 = sum[:num_sum]
    sum2 = sum[num_sum:]

    pipei = fuzz.token_sort_ratio(sum1, sum2)

    num_sales = 4000
    num_comments = 512
    my_ratio = num_comments / num_sales
    resultstr = ""
    if 0.18 < my_ratio < 0.28:
        print('该商品评论真实性很好！')
        resultstr = '该商品评论真实性很好！'
    elif my_ratio <= 0.18:
        print('该商品评论真实性很好！')
        resultstr = '该商品评论真实性很好！'
    elif 0.28 <= my_ratio <= 0.38:
        print('该商品评论真实性尚可。')
        resultstr = '该商品评论真实性尚可！'
    else:
        print('该商品评论真实性较差。')
        resultstr = '该商品评论真实性较差！'

    if 0 < pipei < 15:
        print('推荐购买！')
        resultstr += " 推荐购买"
    elif 15 <= pipei <= 30:
        print('可根据实际需求酌情购买。')
        resultstr += " 可根据实际需求酌情购买"
    else:
        print('商家有刷单嫌疑，请谨慎购买！')
        resultstr += " 商家有刷单嫌疑，请谨慎购买！"

    from util import comment

    import time
    time.sleep(2) #暂停2秒吧

    from util import youdian

    from util import quedian

    return render(request, "result.html", locals());
