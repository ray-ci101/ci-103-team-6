from pymysql import *
import matplotlib.pyplot as plt  # 数据可视化
from wordcloud import WordCloud, ImageColorGenerator, STOPWORDS, get_single_color_func  # 词云
import numpy as np  # 科学计算
from PIL import Image  # 处理图片

conn = connect(host='localhost', port=3306, user='root', password='We200219741012', database='fakespy',
               charset='utf8')
cur = conn.cursor()  ##获取游标
cur.execute('select keyword from wordcloud')
keywords = cur.fetchall()
cur.execute('select EMO from wordcloud')
emotion = cur.fetchall()
print(emotion)
print(keywords)
conn.commit()
conn.close()
print('OK')


def draw_cloud(graph):
    good = []
    backgroud = np.array(Image.open(graph))  # 背景轮廓图
    mywordcloud = WordCloud(background_color="white",  # 背景颜色
                            mask=backgroud,  # 写字用的背景图，从背景图取颜色
                            max_words=300,  # 最大词语数量
                            stopwords=STOPWORDS,  # 停用词
                            font_path="simkai.ttf",  # 字体
                            max_font_size=100,  # 最大字体尺寸
                            random_state=50,  # 随机角度
                            width=1000,
                            height=700,
                            scale=1.5,
                            )
    for j in range(len(keywords)):
        key1 = str(keywords[j])
        if emotion[j] == (2,):
            good.append(key1)
    print(good)
    good = " ".join(good)
    wordcloud1 = mywordcloud.generate(good)  # 生成词云

    ImageColorGenerator(backgroud)  # 生成词云的颜色
    # plt.imsave(save_name, mywordcloud)  # 保存图片
    plt.imshow(wordcloud1)
    plt.axis("off")  # 关闭保存
    plt.show()
    wordcloud1.to_file("优点.jpg")  # 保存为图片


draw_cloud(graph="ciyun.jpg")
