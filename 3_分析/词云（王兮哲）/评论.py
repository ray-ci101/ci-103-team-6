import json
from urllib.error import URLError

from urllib.request import urlopen, Request
from pymysql import *


def get_token():
    req = Request(Token_url)
    req.add_header('Content-Type', 'application/json; charset=UTF-8')
    try:
        f = urlopen(req, timeout=5)
        result_str = f.read().decode('utf-8')
    except URLError as err:
        print(err)
    result = json.loads(result_str)
    return result['access_token']


keywords = []
emotion = []


def analysis_comment(host, comment):
    # 定义分析类别（购物）
    data = json.dumps(
        {
            "text": comment,
            "type": 12
        })

    request = Request(url=host, data=data.encode('utf-8'))
    request.add_header('Content-Type', 'application/json; charset=UTF-8')
    response = urlopen(request)
    content = response.read().decode('utf-8')
    rdata = json.loads(content)

    # print("--------------------------------------------------------------")
    # print("评论：")
    # print("    " + comment)
    # print("\n评论关键字：")
    for i in rdata["items"]:
        keywords.append(i['prop'] + i['adj'])
        emotion.append(i['sentiment'])
        # if i['sentiment'] == 2:
        #     print(u"    积极的评论关键词：" + i['prop'] + i['adj'])
        # if i['sentiment'] == 0:
        #     print(u"    消极的评论关键词：" + i['prop'] + i['adj'])
    return keywords, emotion


# 连接数据库调取数据
conn = connect(host='localhost', port=3306, user='root', password='We200219741012', database='fakespy',
               charset='utf8')
cur = conn.cursor()  ##获取游标
cur.execute('select comment from orindata')
result = cur.fetchall()
result = list(result)
for i in range(len(result)):
    result[i] = str(result[i])
print(result)

if __name__ == '__main__':
    Comment_url = "https://aip.baidubce.com/rpc/2.0/nlp/v2/comment_tag_custom"
    Token_url = "https://aip.baidubce.com/oauth/2.0/token?grant_type=client_credentials&client_id=Plj6iLB4pgAwoxuX3uZr0hXM&client_secret=baNoLydBF0G97cqOSonipilw2cC8O1nL"

    A_t = get_token()
    host = Comment_url + "?charset=UTF-8&access_token=" + A_t
    comments = []
    for j in range(len(result)):
        try:
            comments.append(result[j])
            analysis_comment(host, comments[j])
        except:
            pass
            # print('无相应词语匹配')

print(keywords)


def update():
    conn = connect(host='localhost', port=3306, user='root', password='We200219741012', database='fakespy',
                   charset='utf8')
    cur = conn.cursor()  ##获取游标
    # sql = ' insert into wordcloud (keyword,EMO) values(%s,%s)'
    cur.execute('delete from wordcloud')
    for k in range(0, len(keywords)):
        cur.execute('insert into wordcloud (keyword,EMO) values(%s,%s)', (keywords[k], emotion[k]))
    conn.commit()
    conn.close()
    print('OK')


update()
