import requests
import json
import re
from pymysql import *


# tianmaocomments = []
# taobaocomments = []
# informations = []
# time = []
# username = []


class TianMao:
    lastPage = 1
    url = "https://rate.tmall.com/list_detail_rate.htm"  # 评论储存源码
    header = {
        "cookie": 'cna=Qd9XFtCKl2ICARu8+Sn5h6X9; hng=CN%7Czh-CN%7CCNY%7C156; lid=tb713822123; enc=dV%2B5umGG%2B3b3mzk68mU1uEWlocWD%2FF8pi6XPeJtn05eDnvQarMmI1WWgCMWvf5jQMsZym8UWDDtvn7DVvxTrCusFyqljx0Fjowpg6PpusG0%3D; sm4=620100; sgcookie=E100AuGxB4XkH5njNxDgSj%2FVewF2wHQ7KFORrkx7zQkxZLNh2YXhNZWpMfKpSYb3C3MSHD3cfxp%2BxAraqO%2Bb2Zg4zw%3D%3D; uc1=cookie14=Uoe2zstioy2hQg%3D%3D; t=b8cf5d87aac66bdd0d3066cb549646e4; uc3=id2=UUphw2eV%2BznE9dZRsA%3D%3D&vt3=F8dCuw74BcfOPbnup80%3D&lg2=WqG3DMC9VAQiUQ%3D%3D&nk2=F5RCZI4Wefnd7Gk%3D; tracknick=tb713822123; uc4=id4=0%40U2grGNhsD9joh%2F%2BL8bP9eAerYeiVJvMh&nk4=0%40FY4JikN4UMbwu16jLnYgpfwsP7XNfA%3D%3D; lgc=tb713822123; _tb_token_=ee1fe9e356314; cookie2=210522a8e3d1a2a20783177eafeddfa6; xlly_s=1; x5sec=7b22726174656d616e616765723b32223a2237623662613666653431643765356463373063663163666134313565653265644350797967595947454e756d6a4c2b4669717533655367434d4f32352f397a2b2f2f2f2f2f77453d227d; tfstk=cQzNBbb3nNQwht0f2Vg21vO9x4mOZy0mypljjkS8i58GRjoGiKJxt69xY09Lpcf..; l=eB_udHS4jGTRbjIvBOfZhurza77OSIRx5uPzaNbMiOCPO9CH4mG1W6OACbTMCnhVh68DR3Pp0J00BeYBqnV0x6aNa6Fy_Ckmn; isg=BIqKY4xG3gVp51Ldnl18oQ9N23Asew7VMVG-WRTDNl14xyqB_Ate5dAx1jMbSoZt',
        "referer": "https://detail.tmall.com/item.htm?id=631183184957&ali_refid=a3_430673_1006:1152166747:N:LO74Bc2EWLnfvdjOI7AADg==:b673dcc489d6a23844d447e01c2528fb&ali_trackid=1_b673dcc489d6a23844d447e01c2528fb&spm=a2e0b.20350158.31919782.5",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 Edg/91.0.864.41",
    }
    params = {  # 商品必带信息
        "itemId": "627888586473",  # 商品id
        "sellerId": "3871253083",
        "currentPage": "0",  # 页码
        "order": "3",  # 排序方式:1:按时间降序  3：默认排序
        "callback": "jsonp605",
    }

    def __init__(self, id: str):  # 获取商品id的函数
        self.params['itemId'] = id

    def getPageData(self, pageIndex: int):  # 获取商品评论信息响应信息的函数
        self.params["currentPage"] = str(pageIndex)
        req = requests.get(self.url, self.params, headers=self.header, timeout=2).content.decode(
            'utf-8')  # 解码，并且去除str中影响json转换的字符（\n\rjsonp(...)）;
        req = req[req.find('{'):req.rfind('}') + 1]
        return json.loads(req)

    def setOrder(self, way: int):  # 爬取评论的排序方式
        self.params["order"] = way

    def getAllData(self):  # 循环爬取页面上展现的商品评论
        Data = self.getPageData(1)
        self.lastPage = Data['rateDetail']['paginator']['lastPage']
        for i in range(2, self.lastPage + 1):
            Data['rateDetail']['rateList'].extend(self.getPageData(i)['rateDetail']['rateList'])
        return Data


class TaoBao:
    lastPage = 1
    url = "https://rate.taobao.com/feedRateList.htm"
    header = {
        "cookie": 'cna=Qd9XFtCKl2ICARu8+Sn5h6X9; t=b8cf5d87aac66bdd0d3066cb549646e4; sgcookie=E100AuGxB4XkH5njNxDgSj%2FVewF2wHQ7KFORrkx7zQkxZLNh2YXhNZWpMfKpSYb3C3MSHD3cfxp%2BxAraqO%2Bb2Zg4zw%3D%3D; uc3=id2=UUphw2eV%2BznE9dZRsA%3D%3D&vt3=F8dCuw74BcfOPbnup80%3D&lg2=WqG3DMC9VAQiUQ%3D%3D&nk2=F5RCZI4Wefnd7Gk%3D; lgc=tb713822123; uc4=id4=0%40U2grGNhsD9joh%2F%2BL8bP9eAerYeiVJvMh&nk4=0%40FY4JikN4UMbwu16jLnYgpfwsP7XNfA%3D%3D; tracknick=tb713822123; _cc_=VT5L2FSpdA%3D%3D; enc=dV%2B5umGG%2B3b3mzk68mU1uEWlocWD%2FF8pi6XPeJtn05eDnvQarMmI1WWgCMWvf5jQMsZym8UWDDtvn7DVvxTrCusFyqljx0Fjowpg6PpusG0%3D; hng=CN%7Czh-CN%7CCNY%7C156; thw=cn; mt=ci=-1_0; _tb_token_=ee1fe9e356314; _m_h5_tk=0cd34fa1e5a7e7bc215f51a8c19c9cb8_1623226431692; _m_h5_tk_enc=a8d05d92e5a6508d242ac97d3e283900; xlly_s=1; cookie2=210522a8e3d1a2a20783177eafeddfa6; _samesite_flag_=true; v=0; uc1=cookie14=Uoe2zstioy8DAA%3D%3D; tfstk=cujcBV11IZ8Xq_YlRotfHbwj7wXcZKe2M15f4GkdxgFC_psPiPur8rMYrQ2MEV1..; l=eBxNMSjqjGTbPsXFBO5Courza77TmIRb8cNzaNbMiInca1oAMQZAGNCBMO8wydtxgtfxLeKz9nynoRhMJjaU-ETjGO0qOC0eQxv9-; isg=BBISx13Cpm0i4Npl1rTu_sDzY9j0Ixa9eYk2wdxrskWG77PpxbOSzP9JXktT7I5V',
        "referer": "https://item.taobao.com/item.htm?spm=a21bo.21814703.201876.31.184111d9X9VPdP&id=615694554627&scm=1007.34127.211940.0&pvid=b11f3167-9578-423d-a8f3-757cae39ffea",
        "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.77 Safari/537.36 Edg/91.0.864.41",
    }
    params = {  # 必带信息
        'auctionNumId': '615694554627',  # 商品id
        'userNumId': '246482200',
        "currentPageNum": "0",  # 页码
        "order": "1",  # 排序方式:1:按时间降序  3：默认排序
        "callback": "jsonp1761",
    }

    def __init__(self, id: str):  # 获取商品id的函数
        self.params['auctionNumId'] = id

    def getPageData(self, pageIndex: int):  # 获取商品评论信息响应信息的函数
        self.params["currentPageNum"] = str(pageIndex)
        req = requests.get(self.url, self.params, headers=self.header, timeout=2).content.decode(
            'utf-8')  # 解码，并且去除str中影响json转换的字符（\n\rjsonp(...)）;
        req = req[req.find('{'):req.rfind('}') + 1]
        return json.loads(req)

    def setOrder(self, way: int):  # 爬取评论的排序方式
        self.params["order"] = way

    def getAllData(self):  # 循环爬取页面上展现的商品评论
        Data = self.getPageData(1)
        self.lastPage = Data['maxPage']
        for i in range(2, self.lastPage + 1):
            Data['comments'].extend(self.getPageData(i)['comments'])
        return Data


url1 = input('请输入商品URL：')
index1 = re.search('id', url1).span()  # 检索用户输入URL中的商品id索引
if index1[-1] > 70:  # 用以判断该商品来源为天猫还是淘宝（天猫淘宝所需的header不同）
    index2 = index1[-1] + 1
    list1 = list(url1)
    id = ''
    for i in range(index2, index2 + 12):
        id += list1[i]  # 循环以获取商品id
    taobao = TaoBao(id)
    k = taobao.getAllData()
    k = str(k)
    taobaocomments = re.findall(r'\'content\': \'(.+?)\',', k)  # 正则表达式匹配所需的商品评论
    # taobaocomments = taobaocomments.replaceAll("[\\x{10000}-\\x{10FFFF}]", "")  # 去除评论的表情符号，以免导入数据库出错
    # print(taobaocomments)
    informations = re.findall(r'\'sku\': \'(.+?)\',', k)  # 正则表达式匹配所需的商品种类
    # print(informations)
    time = re.findall(r'\'date\': \'(.+?)\',', k)  # 正则表达式匹配所需的评论日期
    # print(time)
    username = re.findall(r'\'nick\': \'(.+?)\',', k)  # 正则表达式匹配所需的用户名称
    # print(username)

else:
    index2 = index1[-1] + 1
    list1 = list(url1)
    id = ''
    for i in range(index2, index2 + 12):
        id += list1[i]
    tianmao = TianMao(id)
    k = tianmao.getAllData()
    k = str(k)
    tianmaocomments = re.findall(r'\'rateContent\': \'(.+?)\',', k)
    # tianmaocomments = tianmaocomments.replaceAll("[\\x{10000}-\\x{10FFFF}]", "")
    # print(tianmaocomments)
    informations = re.findall(r'\'auctionSku\': \'(.+?)\',', k)
    # print(informations)
    time = re.findall(r'\'rateDate\': \'(.+?)\',', k)
    # print(time)
    username = re.findall(r'\'displayUserNick\': \'(.+?)\',', k)
    # print(username)


def update():
    conn = connect(host='localhost', port=3306, user='root', password='We200219741012', database='fakespy',
                   charset='utf8mb4')
    cur = conn.cursor()  ##获取游标
    # sql = ' insert into wordcloud (keyword,EMO) values(%s,%s)'
    cur.execute('truncate table orindata')
    for p in range(0, len(username)):
        cur.execute('insert into orindata (comment,productype,cTime,username) values(%s,%s,%s,%s)',
                    (tianmaocomments[p], informations[p], time[p], username[p]))
    conn.commit()
    conn.close()
    print('OK')


update()
